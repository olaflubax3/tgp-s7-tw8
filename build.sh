#!/bin/bash
# TGP Build Script for Linux v1.22 by djb77 / XDA-Developers

export rootdir=$(pwd)
export tgpdir=$rootdir/build/tgp
export tgpfirmware=$(<tools/firmware)
export tgpversion=$(<tools/version)
export zipname='TGP_G93xx_'$tgpfirmware'_v'$tgpversion'.zip'

echo ""
echo "TGP build script by @djb77"
echo "--------------------------"
echo ""
echo "Backing up folders"
[ -d "$rootdir/backup" ] && rm -rf $rootdir/backup
mkdir $rootdir/backup
cp -rf $tgpdir/kernels $rootdir/backup/kernels
cp -rf $tgpdir/recoveries $rootdir/backup/recoveries
cp -rf $tgpdir/modems $rootdir/backup/modems

echo ""
echo "Packing Kernels"
cd $tgpdir/kernels
tar -cf - * | xz -9 -c - > kernels.tar.xz
rm -f *.img
echo "Packing Recoveries"
cd $tgpdir/recoveries
tar -cf - * | xz -9 -c - > recovery.tar.xz
rm -f *.img
echo "Packing Modems"
cd $tgpdir/modems/g930f
echo "- G930F Modem"
tar -cf - * | xz -9 -c - > modem.tar.xz
rm -f *.bin
cd $tgpdir/modems/g930w8
echo "- G930W8 Modem"
tar -cf - * | xz -9 -c - > modem.tar.xz
rm -f *.bin
cd $tgpdir/modems/g935f
echo "- G935F Modem"
tar -cf - * | xz -9 -c - > modem.tar.xz
rm -f *.bin
cd $tgpdir/modems/g935w8
echo "- G935W8 Modem"
tar -cf - * | xz -9 -c - > modem.tar.xz
rm -f *.bin
echo ""
echo "Building Zip File $zipname"
cd $rootdir/build
zip -9gq $zipname -r META-INF/ -x "*~"
zip -9gq $zipname -r tgp/ -x "*~"
mv $zipname $rootdir/$zipname
cd $rootdir
chmod a+r $zipname

echo ""
echo "Restoring Folders"
rm -rf $tgpdir/kernels
rm -rf $tgpdir/recoveries
rm -rf $tgpdir/modems
cp -rf $rootdir/backup/kernels $tgpdir/kernels
cp -rf $rootdir/backup/recoveries $tgpdir/recoveries
cp -rf $rootdir/backup/modems $tgpdir/modems
[ -d "$rootdir/backup" ] && rm -rf $rootdir/backup
echo ""
echo "Done."
echo ""

